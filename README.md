# 雑談apiテスト

あらかじめcoffeeを入れておく
あとdocomo apiを取得しておく

## setup

install request

    $ npm install request

export environment variable

    $ export DOCOMO_API_KEY=AAAAAAAA

## execute

    $ coffee docomotest.coffee

## see

https://dev.smt.docomo.ne.jp/?p=docs.api.page&api_docs_id=17#tag01
